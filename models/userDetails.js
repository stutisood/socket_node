const mongoose = require('mongoose');

const detailSchema = new mongoose.Schema({

   username: {
    type: String
   },

   password: {
    type: String
   },

   email: {
      type: String
   },

   // jti: {
   //    type: String
   // }
},

{timestamps: true}

) 

const detailModel = mongoose.model('detailModel', detailSchema);
module.exports= detailModel