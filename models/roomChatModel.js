const mongoose = require('mongoose');

const roomChatSchema = new mongoose.Schema({

    senderId: {
        type: String
    },

    recieverId: {
        type: String
    }

},
{timestamps: true}
) 

const roomChatModel = mongoose.model('roomChatModel', roomChatSchema);
module.exports= roomChatModel