const mongoose = require('mongoose');

const chatSchema = new mongoose.Schema({

    // roomId: {
    //     type: String
    // },

    roomId: {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'roomChatModel'
    },

    message: {
        type: String
    }
},

{timestamps: true}

) 

const chatModel = mongoose.model('chatModel', chatSchema);
module.exports= chatModel