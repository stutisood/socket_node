const express = require('express');
const http = require('http')

const router = require('./v1/routes/route')
const app = express();

app.use(express.json());
app.use('/',router);

const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const { createSwagger } = require('./service/swagger');

const server = http.createServer(app)
const io = require('socket.io')(server)
const socket = require("./sockets/socket");
require('./common/connection');

const swaggerDocument = YAML.load('./swagger/collection.yml');

// server.listen(5000,() => {
//     socket(io);
//     console.log('server started at port 5000')
// })

server.listen(5000, async() => {
    await createSwagger();
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
})



// app.listen(5000, async()=> {
    
// });

