const joi = require('joi')

const userValidate = joi.object({
    username: joi.string().min(3).max(30).required(),
    password: joi.string().required(),
    // lastName: joi.string().min(3).max(30).required(),
    email: joi.string().email().required(),
    // dialCode: joi.string().required(),
    // phoneNumber: joi.string().required().length(10).pattern(/^[0-9]+$/),
});

module.exports = userValidate