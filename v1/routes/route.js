const express = require('express')
const controller = require("../controller/apiMethods")
const router = express.Router();

router.post("/postDetails", controller.postDetails)
router.get("/getAllChats", controller.getAllChats)
router.get("/getChat/:id",controller.getChat)
router.get("/aggregate", controller.aggregate)


module.exports=router