const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const chatModel = require('../../models/chatModel')
const detailModel = require('../../models/userDetails')
const detailValidate = require('../validation/validation')
const roomChatModel = require('../../models/roomChatModel')
//const { agenda } =  require('../../service/sendMail')
require('../../common/connection')

const secretKey = "secretKey"

module.exports.postDetails = async(req, res) => {
   //console.log("hello")
   //console.log(req.body.username)
   await detailValidate.validateAsync(req.body);

   let password = req.body.password;
   let hashPassword = await bcrypt.hash(password,10);

    const data = await detailModel.create({
        username: req.body.username,
        email: req.body.email,
        password: hashPassword
        
    });
    //console.log(data);

    // jwt.sign({ _id }, secretKey, { expiresIn: '1h'}, (err, token) => {
    //     res.json({
    //         token
    //     })
    // })
    // const emailId = req.body.email;

    // (async function () {
    //   await agenda.schedule("in 1 minute", "send email report", {
    //     to: emailId,
    //   });
    // })();

    return res.json({
        data
    }) 
}

module.exports.getAllChats = async(req, res) => {

    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);
    const skip = (page - 1) * limit;

    const chats = await chatModel.find()
    .skip()
    .limit()

 
    //Projection
    //const chats = await chatModel.find({}, {message : 1});
    res.json({chats});
}


module.exports.getChat = async(req, res) => {
    id = req.params.id
   //console.log(id);
    const chats = await chatModel.findOne({roomId : id}).sort({"createdAt" : -1})
    res.json({chats})
}


module.exports.aggregate = async(req, res) => {
    const aggregate = await roomChatModel.aggregate([

        // {
        //     $match: {studentId:userid}
        //   },
    
        {
            $lookup : {
                from: "chatModel",
                localField: "_id",
                foreignField: "roomId",
                as: "chats",
            }
        },

        {
            $unwind:"$chats"
        },

        {
            $sort: {createdAt: -1}
        },


        {
            $project: {
               //"chats._id" : false,
               "_id" : false
            }
        }

    ]) 
    return res.json({aggregate})
}

